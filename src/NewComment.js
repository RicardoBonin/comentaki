import React, { useState, useContext }from 'react'
import {useDatabasePush} from './database'
import firebase from './firebase'
import { AuthContext} from './auth'

const NewComment = () => {
  const [, save] = useDatabasePush('comments')
  const [comment, setComment] = useState('')
  const auth = useContext(AuthContext)
  if(auth.user === null) {
    return null
  }
  const {displayName} = auth.user
  const [aleternativeDisplayName] = auth.user.email.split('@')
  const creatComment = () => {
    if (comment !== '') {
      save({
        content: comment,
        createdAt: firebase.database.ServerValue.TIMESTAMP,
        user: {
          id: auth.user.uid,
          name: displayName || aleternativeDisplayName
        }
      })
      setComment('')
    }
  }
  return (
    <div>
      <textarea value={comment} maxLength='500' rows='10' placeholder='Comente aqui!' onChange={evt => setComment(evt.target.value)} />
      <br />
      <button className='btn' onClick={creatComment}>Comentar!</button>
    </div>
  )
}
export default NewComment