import React from 'react'
import UserInfo from './UserInfo'
import { AuthProvider } from './auth'
import { auth } from 'firebase'


const Header = () => {
  return (
    <AuthProvider>
      <div className='header'>
        <div>
          <h1>Comenta aqui!</h1></div>
        <div>
            <UserInfo />
          </div>
        
      </div>
    </AuthProvider>
    
  )
}

export default Header