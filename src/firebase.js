import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyBjk6W8c0irr--pG-lm3tTkt2mIFVBz-tY",
  authDomain: "comentaki-rb.firebaseapp.com",
  databaseURL: "https://comentaki-rb.firebaseio.com",
  projectId: "comentaki-rb",
  storageBucket: "comentaki-rb.appspot.com",
  messagingSenderId: "888923468495",
  appId: "1:888923468495:web:b939b1bc286037374c3dba"
}
firebase.initializeApp(firebaseConfig)

export default firebase