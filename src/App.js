import React from 'react'
import { Route, BrowserRouter as Router} from 'react-router-dom'
import './App.css'
import Home from './Home'
import Header from './Header';
import Footer from './Footer';
import CreateLogin from './createLogin';



function App() {
  return (
    <Router>
      <div className='test'>
        <Header />
        <Route path='/' exact component={Home} />
        <Route path='/createlogin' exact component={CreateLogin} />
        <Footer />
      </div>
    </Router>
    
  );
}

export default App;
