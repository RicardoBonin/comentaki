import React from 'react'

import NewComment from './NewComment'
import Comments from './Comments'
import CreateUser from './CreateUser'
import UserInfo from './UserInfo'
import { AuthProvider } from './auth'
import SignInUser from './SignInUser'

const Home = () =>{
  return(
    <AuthProvider>
      <div className='home'>
        <SignInUser />
        <Comments />
        <NewComment />
      </div>
    </AuthProvider>
  )
}
export default Home