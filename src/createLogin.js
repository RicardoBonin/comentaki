import React from 'react'
import { AuthProvider } from './auth'
import CreateUser from './CreateUser'


const CreateLogin = () => {
  return (
    <AuthProvider>
      <CreateUser />
    </AuthProvider>
  )
}

export default CreateLogin