import React, { useContext, useState } from 'react'
import { AuthContext } from './auth'

const SignInUser = () => {
  const auth = useContext(AuthContext)
  const [form, setForm] = useState({
    email: '',
    passwd: ''
  })
  const onChange = campo => evt => {
    setForm({
      ...form,
      [campo]:evt.target.value
    })
  }
  if (auth.user !== null){
    return null
  }
  return (
    <div className='containerSignin'>
      <div className='form-signin'>
        <img src='https://www.petropolispaulista.com.br/assets/img/icone-login-dark.svg'/>
        <h3>Faça Seu Login</h3>
        <form>
          <input type='text' placeholder='Seu e-mail' value={form.email} onChange={onChange('email')} />
          <input type='password' placeholder='Sua senha' value={form.passwd} onChange={onChange('passwd')} />
          <button onClick={(e)=> {
          auth.signInUser.signInUser(form.email, form.passwd)
          e.preventDefault()
        }}>Entrar</button>
        </form>
        <div>
        {
          auth.signInUser.signInUserState.error !== '' &&
          <p>{auth.signInUser.signInUserState.error}</p>
        }
        </div>
      </div>
        <div className='conteudo'>
          <h1>Comente aqui!</h1>
          <p>O que você está achando desse módulo? O que mais chamou sua atenção? Fico alguma dúvida?</p>
        </div>
      </div>

  )
}
export default SignInUser