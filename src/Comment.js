import React, {useContext } from 'react'
import Time from './Time'
import { AuthContext } from './auth'
const Comment = ({ comment }) => {
  const auth = useContext(AuthContext)
  return (
    <div className='comment'>
      <div>
        <img src='https://www.petropolispaulista.com.br/assets/img/icone-login-dark.svg' />
      </div>
      <div>
        <p>{comment.content}</p>
        <p>por: {comment.user.name}</p>
        em: <Time timestamp={comment.createdAt} />
      </div>
     </div>
  )
}

export default Comment  